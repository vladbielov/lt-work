region                      =   "us-east-1"
cidr_block                  =   "10.0.0.0/16"

private_cidr_b               =   "10.0.0.0/24"
private_cidr_c               =   "10.0.1.0/24"
private_cidr_d               =   "10.0.2.0/24"
private_cidr_e               =   "10.0.3.0/24"

public_cidr_b                =   "10.0.4.0/24"
public_cidr_c                =   "10.0.5.0/24"
public_cidr_d                =   "10.0.6.0/24"
public_cidr_e                =   "10.0.7.0/24"

max_size                    =   "2"
min_size                    =   "1"
desired_capacity            =   "2"


tags    =   {
    Name                    =   "LT_Lotsa_Project"
    Environment             =   "Dev"

   }