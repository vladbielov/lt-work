resource "aws_subnet" "private_b" {
  vpc_id            = "${aws_vpc.lotsa-vpc.id}"
  cidr_block        = "${var.private_cidr_b}"
  availability_zone = "${var.region}b"
  tags              = {
      Name          = "LT-WS Private Subnet B"
  }
}

resource "aws_subnet" "private_c" {
  vpc_id            = "${aws_vpc.lotsa-vpc.id}"
  cidr_block        = "${var.private_cidr_c}"
  availability_zone = "${var.region}c"
  tags              = {
      Name          = "LT-WS Private Subnet C"
  }
}

resource "aws_subnet" "private_d" {
  vpc_id            = "${aws_vpc.lotsa-vpc.id}"
  cidr_block        = "${var.private_cidr_d}"
  availability_zone = "${var.region}d"
  tags              = {
      Name          = "LT-WS Private Subnet D"
  }
}

resource "aws_subnet" "private_e" {
  vpc_id            = "${aws_vpc.lotsa-vpc.id}"
  cidr_block        = "${var.private_cidr_e}"
  availability_zone = "${var.region}e"
  tags              = {
      Name          = "LT-WS Private Subnet E"
  }
}

resource "aws_subnet" "public_b" {
  vpc_id                  = "${aws_vpc.lotsa-vpc.id}"
  cidr_block              = "${var.public_cidr_b}"
  availability_zone       = "${var.region}b"
  map_public_ip_on_launch = true
  tags                    = {
      Name                = "LT-WS Public Subnet B"
  }
}

resource "aws_subnet" "public_c" {
  vpc_id                  = "${aws_vpc.lotsa-vpc.id}"
  cidr_block              = "${var.public_cidr_c}"
  availability_zone       = "${var.region}c"
  map_public_ip_on_launch = true
  tags                    = {
      Name                = "LT-WS Public Subnet C"
  }
}

resource "aws_subnet" "public_d" {
  vpc_id                  = "${aws_vpc.lotsa-vpc.id}"
  cidr_block              = "${var.public_cidr_d}"
  availability_zone       = "${var.region}d"
  map_public_ip_on_launch = true
  tags                    = {
      Name                = "LT-WS Public Subnet D"
  }
}

resource "aws_subnet" "public_e" {
  vpc_id                  = "${aws_vpc.lotsa-vpc.id}"
  cidr_block              = "${var.public_cidr_e}"
  availability_zone       = "${var.region}e"
  map_public_ip_on_launch = true
  tags                    = {
      Name                = "LT-WS Public Subnet E"
  }  
}
