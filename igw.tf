resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.lotsa-vpc.id}"
  tags   = "${var.tags}"
}