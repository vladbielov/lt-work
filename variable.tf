variable "region" {}
variable "cidr_block" {}
variable "public_cidr_b" {}
variable "public_cidr_c" {}
variable "public_cidr_d" {}
variable "public_cidr_e" {}
variable "private_cidr_b" {}
variable "private_cidr_c" {}
variable "private_cidr_d" {}
variable "private_cidr_e" {}
variable "max_size" {}
variable "min_size" {}
variable "desired_capacity" {}

variable "tags" {
  type = map
}