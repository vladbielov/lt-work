# Security group for "Web Load Balancer"
resource "aws_security_group" "web_lb_sg" {
  name        = "web_lb_sg"
  vpc_id      = "${aws_vpc.lotsa-vpc.id}"

  ingress {
    description = "Allow http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }  
}

# LoadBalancer rule
#resource "aws_security_group_rule" "web_lb_rule" {
#  type              = "ingress"
#  from_port         = 0
#  to_port           = 0
#  protocol          = "-1"
#  security_group_id = "${aws_security_group.web_lb_sg.id}"
#}

# Security group for "Master Web Server v3"
resource "aws_security_group" "master_web" {
  name        = "master_web"
  vpc_id      = "${aws_vpc.lotsa-vpc.id}"

#  ingress_rules     = "${aws_security_group_rule.web_lb_rule.id}"
 
  ingress  {
    description = "Allow http traffic"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress  {
    description = "Allow https traffic"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
      Name      = "LT-Master Web Server v3 SG"
  }
}

# Security group for "Web Server (ASG)"
resource "aws_security_group" "web_server_asg" {
  name        = "web_server_asg"
  vpc_id      = "${aws_vpc.lotsa-vpc.id}"

  ingress {
    description = "Allow ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }

    ingress  {
    description = "Allow http traffic"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress  {
    description = "Allow https traffic"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
      Name      = "LT-Web Server SG (ASG)"
  }
}

# Security group for "Job Server (NATSG)"
resource "aws_security_group" "job_server" {
  name        = "job_server"
  vpc_id      = "${aws_vpc.lotsa-vpc.id}"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["73.210.80.174/32"]
  }  

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
      Name      = "LT-Job Server SG"
  }
}