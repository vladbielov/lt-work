resource "aws_route_table" "prv_rt_e" {
  vpc_id = "${aws_vpc.lotsa-vpc.id}"

  route = []  
#  route {
#    cidr_block = "0.0.0.0/0"
#    nat_gateway_id = "${aws_nat_gateway.ngw.id}"
#    gateway_id = "${aws_internet_gateway.gw.id}"
#  }

  tags = {

      Name = "LT-WS Private Subnet E"
  }
}

resource "aws_route_table_association" "prv_rt_as_e" {
  subnet_id      = "${aws_subnet.private_e.id}"
  route_table_id = "${aws_route_table.prv_rt_e.id}"
}

#
resource "aws_route_table" "prv_rt_c" {
  vpc_id = "${aws_vpc.lotsa-vpc.id}"

  route = []  

  tags = {

      Name = "LT-WS Private Subnet C"
  }
}

resource "aws_route_table_association" "prv_rt_as_c" {
  subnet_id      = "${aws_subnet.private_c.id}"
  route_table_id = "${aws_route_table.prv_rt_c.id}"
}
#
resource "aws_route_table" "prv_rt_d" {
  vpc_id = "${aws_vpc.lotsa-vpc.id}"

  route = []  

  tags = {

      Name = "LT-WS Private Subnet D"
  }
}

resource "aws_route_table_association" "prv_rt_as_d" {
  subnet_id      = "${aws_subnet.private_d.id}"
  route_table_id = "${aws_route_table.prv_rt_d.id}"
}
#
resource "aws_route_table" "pub_rt" {
  vpc_id = "${aws_vpc.lotsa-vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }
  
  tags = {

      Name = "LT-Website Public Route Table"
  }

}

resource "aws_route_table_association" "pub_rt_as_b" {
  subnet_id      = "${aws_subnet.public_b.id}"
  route_table_id = "${aws_route_table.pub_rt.id}"
}

resource "aws_route_table_association" "pub_rt_as_c" {
  subnet_id      = "${aws_subnet.public_c.id}"
  route_table_id = "${aws_route_table.pub_rt.id}"
}

resource "aws_route_table_association" "pub_rt_as_d" {
  subnet_id      = "${aws_subnet.public_d.id}"
  route_table_id = "${aws_route_table.pub_rt.id}"
}

resource "aws_route_table_association" "pub_rt_as_e" {
  subnet_id      = "${aws_subnet.public_e.id}"
  route_table_id = "${aws_route_table.pub_rt.id}"  
}