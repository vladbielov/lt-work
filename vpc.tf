resource "aws_vpc" "lotsa-vpc" {
  cidr_block = "${var.cidr_block}"
  tags       = "${var.tags}"
}